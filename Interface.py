#encoding: utf-8
import tkinter
import conections

class Interface:
    x = 20
    y = 50
    sockets = conections.conexao()

    def __init__(self):
        self.fonte = ("Arial", "10")
        self.interface = tkinter.Tk('master')
        self.interface.geometry("250x250")
        inicioLabel = tkinter.Label(text="Trabalho do APS", font=self.fonte)
        inicioLabel.place(x=60, y=50)
        inicioBotao = tkinter.Button()
        inicioBotao["text"] = "Iniciar"
        inicioBotao["command"] = lambda: [self.pag2(self.interface), self.interface.withdraw()]
        inicioBotao["pady"] = 30
        inicioBotao["padx"] = 60
        inicioBotao.place(x=50, y=100)

        self.interface.mainloop()

    def widget(self, texto, master, x, y):
        entradaLabel = tkinter.Label(master, text=texto, font=self.fonte)
        entradaLabel.place(x=x, y=y)
        entrada = tkinter.Entry(master)
        entrada.place(x=x+30, y=y)

        return entrada

    def pag2(self, master):
        pag2 = tkinter.Toplevel(master)
        framepag2 = tkinter.Frame(pag2)
        pag2.geometry("250x325")
        framepag2.pack()
        conectarBotao = tkinter.Button(pag2)
        conectarBotao["text"] = "Conectar"
        conectarBotao["command"] = lambda: [self.conect(master), pag2.destroy()]
        conectarBotao["pady"] = 30
        conectarBotao["padx"] = 60
        conectarBotao.place(x=40, y=100)


    def conect(self, master):
        self.sockets.socket()
        self.pag3(master)


    def pag3(self, master):
        pag3 = tkinter.Toplevel(master)
        framepag3 = tkinter.Frame(pag3)
        pag3.geometry("250x325")
        framepag3.pack()

        label = tkinter.Label(pag3, text="Escolha uma op��o abaixo: ")
        label.place(x=50, y=50)
        
        dadosB = tkinter.Button(pag3)
        dadosB["text"] = "Buscar"
        dadosB["command"] = lambda: [self.pag4(master), pag3.destroy()]
        dadosB.place(x=90, y=90)
        dadosB["padx"] = 10
        
        dadosA = tkinter.Button(pag3)
        dadosA["text"] = "Alterar"
        #dadosA["command"] = lambda: [self.page4(master, self.curvas.strponto), pag3.destroy()]
        dadosA["padx"] = 10
        dadosA.place(x=90, y=130)


    def pag4(self, master):
        pag4 = tkinter.Toplevel(master)
        framepag4 = tkinter.Frame(pag4)
        pag4.geometry("250x325")
        framepag4.pack()

        labelv = tkinter.Label(pag4, text="Digite os valores:")
        labelv.place(x=30, y=10)
        id = self.widget("ID:", pag4, 30, 40)
        valor = self.widget("Chave:", pag4, 20, 70)

        enviar = tkinter.Button(pag4)
        enviar["text"] = "Buscar"
        enviar["command"] = lambda: [self.pegaDados(id, valor), self.enviarDados()]
        enviar["padx"] = 10
        enviar.place(x=40, y=180)

        voltar = tkinter.Button(pag4)
        voltar["text"] = "Voltar"
        voltar["command"] = lambda: [self.pag3(master), pag4.destroy()]
        voltar["padx"] = 10
        voltar.place(x=120, y=180)

    def pegaDados(self, id, valor):
        self.sockets.id = id.get()
        self.sockets.valor = valor.get()


    def enviarDados(self):
        self.sockets.enviar()